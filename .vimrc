colorscheme wombat256mod 
syntax enable
set tabstop=4
set softtabstop=4
set number
set cursorline
set wildmenu
set showmatch
let g:instant_markdown_autostart =0
let g:instant_markdown_open_to_the_world = 2
