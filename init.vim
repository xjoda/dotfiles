colorscheme wombat256mod 
syntax enable
set tabstop=2
set softtabstop=2
set number
set cursorline
set wildmenu
set showmatch

call plug#begin('~/.local/share/nvim/site/autoload/')

Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install'  }

Plug 'vimoutliner/vimoutliner'

Plug 'vimwiki/vimwiki'

call plug#end()
